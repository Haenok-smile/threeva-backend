from rest_framework import serializers
from .models import Doctor, Clinics, Review, City


class DoctorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Doctor
        fields = '__all__'

class ClinicsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Clinics
        fields = '__all__'

class ReviewSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Review
        fields = '__all__'

class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'

class ReviewByDoctorIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = '__all__'
