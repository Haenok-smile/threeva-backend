# Generated by Django 2.1.5 on 2019-01-29 16:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doctor',
            name='image',
            field=models.CharField(max_length=200),
        ),
    ]
