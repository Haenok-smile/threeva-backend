from django.shortcuts import render
from django.db import connection
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Doctor, Clinics, Review, City
from .serializers import DoctorSerializer, ClinicsSerializer, ReviewSerializer, CitySerializer, ReviewByDoctorIdSerializer
import datetime,json
from ast import literal_eval


class DoctorView(viewsets.ModelViewSet):
    queryset = Doctor.objects.all()
    serializer_class = DoctorSerializer

class ClinicsView(viewsets.ModelViewSet):
    queryset = Clinics.objects.all()
    serializer_class = ClinicsSerializer

class ReviewView(viewsets.ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer

class CityView(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer


@api_view(['POST'])
def search_doctor(request):
    if request.method == 'POST':
        print(request.data)
        cityname = request.data['city_slug']
        specialities = request.data['specialities']
        region = request.data['region']
        print(cityname, region, specialities)

        # with connection.cursor() as cursor:
        #     cursor.execute(
        #         "SELECT * FROM api_doctor LEFT JOIN api_clinics ON api_doctor.clinics_id = api_clinics.clinics_id WHERE api_clinics.city_slug = '" + cityname + "' " + "AND api_clinics.region_name = '" + region + "' " + "AND api_doctor.specialities LIKE" + " '" + "%" + specialities + "%'"
        #     )
        #     rows = cursor.fetchall()
        
        if region != 'Все районы':
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT * FROM api_doctor LEFT JOIN api_clinics ON api_doctor.clinics_id = api_clinics.clinics_id WHERE api_clinics.city_slug = '" + cityname + "' " + "AND api_clinics.region_name = '" + region + "' " + "AND api_doctor.specialities LIKE" + " '" + "%" + specialities + "%'"
                )
                rows = cursor.fetchall()
        else:
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT * FROM api_doctor LEFT JOIN api_clinics ON api_doctor.clinics_id = api_clinics.clinics_id WHERE api_clinics.city_slug = '" + cityname + "' " + "AND api_doctor.specialities LIKE" + " '" + "%" + specialities + "%'"
                )
                rows = cursor.fetchall()

        # print("Rows",rows)
        result = []

        for row in rows:
            item = {}

            doctorId = row[1]
            review_count = Review.objects.filter(doctor_id=doctorId).count()
            reviews = Review.objects.filter(doctor_id=doctorId)
            review_specializer = ReviewByDoctorIdSerializer(reviews, many=True)

            item['doctor_id']              = row[1]
            item['clinics_id']             = row[2]
            item['name']                   = row[3]
            item['description']            = row[4]
            item['image']                  = row[5]
            item['experience_year']        = row[7]
            item['price']                  = row[8]
            item['rate_value']             = row[9]
            item['education']              = row[10]
            item['work_experience']        = row[11]
            item['serializes_in_diseases'] = row[12]
            item['qualification']          = row[13]
            item['phone']                  = row[16]
            item['lat']                    = row[19]
            item['lng']                    = row[20]
            item['review_count']           = review_count
            item['reviews']                = review_specializer.data
            print(item)
            result.append(item)
            
        # print(json.dumps(result,ensure_ascii=False))

        return Response(json.dumps(result,ensure_ascii=False),status=200)


@api_view(['POST'])
def get_reviews_by_doctor_id(request):
    if request.method == 'POST':
        print(request.data)
        doctorID = request.data['doctor_id']

        review = Review.objects.filter(doctor_id=doctorID)
        print(review)
        review_specializer = ReviewByDoctorIdSerializer(review, many=True)
        return Response(review_specializer.data, status=200)


@api_view(['POST'])
def save_review(request):
    if request.method == 'POST':
        print(request.data)
        doctorId = request.data['doctor_id']
        author = request.data['author']
        rating = request.data['rating']

        review = Review()
        review.doctor_id = doctorId
        review.author = author
        if request.data['text'] is not None:
            review.text = request.data['text']
        review.rating = rating
        review.date = datetime.datetime.now()
        review.save()

        response_data = {'type': 'success'}
        return Response(response_data)


@api_view(['GET'])
def get_city(request):
    if request.method == 'GET':
        city = City.objects.all()
        city_serializer = CitySerializer(city, many=True)
        return Response(city_serializer.data, status=200)


@api_view(['POST'])
def get_region_by_city(request):
    if request.method == 'POST':
        citySlug = request.data['city_slug']

        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT region_name FROM api_clinics WHERE city_slug = '" + citySlug + "' " + " GROUP BY region_name"
            )
            rows = cursor.fetchall()

        return Response(json.dumps(rows,ensure_ascii=False),status=200)


@api_view(['POST'])
def get_speciality(request):
    if request.method == 'POST':
        citySlug = request.data['city_slug']
        region = request.data['region_name']

        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT specialities FROM api_clinics WHERE city_slug = '" + citySlug + "' " + "AND region_name = '" + region + "'"
            )
            rows = cursor.fetchall()
        
        return Response(json.dumps(rows,ensure_ascii=False),status=200)
 

@api_view(['GET'])
def get_doctor_count(request):
    if request.method == 'GET':
        result = []
        queryset = Doctor.objects.values_list('specialities', flat=True)
        for val in queryset:
            arr = literal_eval(val)
            len_arr = len(arr)
            if len_arr != 0:
                result = result + arr
        response_data = {"result":result}
        return Response(response_data, status=200)
