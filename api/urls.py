from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('doctor', views.DoctorView)
router.register('clinics', views.ClinicsView)
router.register('review', views.ReviewView)
router.register('city', views.CityView)

urlpatterns = [ 
    path('', include(router.urls)),
    path('search_doctor', views.search_doctor),
    path('get_reviews_by_doctor_id', views.get_reviews_by_doctor_id),
    path('save_review', views.save_review),
    path('get_city', views.get_city),
    path('get_region_by_city', views.get_region_by_city),
    path('get_doctor_count', views.get_doctor_count),
    path('get_speciality', views.get_speciality)
]